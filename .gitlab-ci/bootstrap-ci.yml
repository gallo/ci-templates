# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:

include:
  - local: '/templates/fedora.yml'

stages:
  - arch base images
  - qemu base images
  - multiarch manifest

#################################################################
#                                                               #
#                    bootstrapping stage                        #
#                                                               #
#################################################################


.bootstrap_skeleton:
  extends: .fdo.container-build@fedora
  stage: arch base images
  variables:
    FDO_UPSTREAM_REPO: freedesktop/ci-templates
    FDO_DISTRIBUTION_VERSION: '34'
    FDO_DISTRIBUTION_TAG: '2021-07-29.0'
    FDO_REPO_SUFFIX: container-build-base
    FDO_CBUILD: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/cbuild/sha256-65e0d92e94a68cbe1a34349e05e9440803d98c01b85b8d6b93ef20c993261e17/cbuild
  before_script: &arch_repo_suffix
    - export FDO_REPO_SUFFIX=$(arch)/$FDO_REPO_SUFFIX


# Builds an image that the .fdo.container-build@ templates run on
# to compose a distribution-specific image.
#
# we need a minimalist image capable of buildah, podman, skopeo, curl,
# jq, date and test.
build container-build-base@x86_64:
  extends: .bootstrap_skeleton
  image: fedora:34
  before_script:
    - *arch_repo_suffix
    - bash bootstrap/bootstrap_fedora.sh

    # check if podman is working currently in our new environment
    - podman info
  variables:
    FDO_DISTRIBUTION_EXEC: 'bash bootstrap/bootstrap_fedora.sh'


# same but for aarch64
build container-build-base@aarch64:
  extends: build container-build-base@x86_64
  image: arm64v8/fedora:34
  tags:
    - aarch64

.combine multiarch:
  extends:
    - .bootstrap_skeleton
  image: $CI_REGISTRY_IMAGE/x86_64/container-build-base:2021-07-29.0
  stage: multiarch manifest
  before_script:
  script:
    # log in to the registry
    - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

    - for ARCH in $FDO_ARCHES ;
      do
        IMAGES="${CI_REGISTRY_IMAGE}/${ARCH}/${FDO_REPO_SUFFIX}:${FDO_DISTRIBUTION_TAG} ${IMAGES}" ;
      done

    # create the multi-arch manifest
    - buildah manifest create ${FDO_REPO_SUFFIX}:${FDO_DISTRIBUTION_TAG}
                              ${IMAGES}

    # check if we already have this manifest in the registry
    - buildah manifest inspect ${FDO_REPO_SUFFIX}:${FDO_DISTRIBUTION_TAG} > new_manifest.json
    - buildah manifest inspect docker://${CI_REGISTRY_IMAGE}/${FDO_REPO_SUFFIX}:${FDO_DISTRIBUTION_TAG} > current_manifest.json || true

    - diff -u current_manifest.json new_manifest.json || touch .need_push

    # and push it
    - |
      if [[ -e .need_push ]]
      then
        rm .need_push
        buildah manifest push --format v2s2 --all \
              ${FDO_REPO_SUFFIX}:${FDO_DISTRIBUTION_TAG} \
              docker://${CI_REGISTRY_IMAGE}/${FDO_REPO_SUFFIX}:${FDO_DISTRIBUTION_TAG}
      fi

combine multiarch:
  extends:
   - .combine multiarch
  variables:
    FDO_ARCHES: x86_64 aarch64
  needs:
    - build container-build-base@aarch64
    - build container-build-base@x86_64


# qemu container base image. This is the base image for **running** qemu jobs.
# The .fdo.qemu-build@ jobs produce a gcow image to start through qemu, that
# image is placed inside this qemu container base image.
#
# The .fdo.distribution-image jobs then run on the qemu base image and can
# start the built virtual machine with the vmctl tool.
#
# Installed required packages (in addition to the bootstrap ones):
# - qemu (of course)
# - genisoimage (to create a cloud-init iso that will help us filling in the custom parameters)
# - usbutils (for being able to call lsusb and redirect part a USB device)
.build qemu run image:
  extends: .bootstrap_skeleton
  stage: qemu base images
  dependencies: []
  variables:
    FDO_DISTRIBUTION_TAG: '2021-07-29.0'
    FDO_REPO_SUFFIX: qemu-base
    FDO_DISTRIBUTION_PACKAGES: 'genisoimage usbutils'
    FDO_DISTRIBUTION_EXEC: 'mkdir -p /app && cp bootstrap/vmctl /app/vmctl'
  before_script:
    - *arch_repo_suffix
    - QEMU_ARCH=$(arch)
    # For x86_64, the qemu package is called qemu-system-x86
    - |
      if [[ $QEMU_ARCH = "x86_64" ]]
      then
        QEMU_ARCH=x86
      fi
    - export FDO_DISTRIBUTION_PACKAGES="qemu-system-${QEMU_ARCH} $FDO_DISTRIBUTION_PACKAGES"
    - export FDO_BASE_IMAGE=$CI_REGISTRY_IMAGE/$(arch)/container-build-base:2021-07-29.0

build qemu run image@x86_64:
  image: $CI_REGISTRY_IMAGE/x86_64/container-build-base:2021-07-29.0
  extends:
    - .build qemu run image
  needs:
    - build container-build-base@x86_64


combine multiarch qemu:
  extends:
   - .combine multiarch
  variables:
    FDO_REPO_SUFFIX: qemu-base
    FDO_DISTRIBUTION_TAG: '2021-07-29.0'
    FDO_ARCHES: x86_64
  needs:
    - build qemu run image@x86_64


# qemu container capable of creating an other VM image. This is the image the
# .fdo.qemu-build@ templates themselves run on.
build qemu-build image@x86_64:
  extends: build qemu run image@x86_64
  tags:
    - kvm
  variables:
    FDO_REPO_SUFFIX: qemu-build-base
    FDO_DISTRIBUTION_EXEC: 'mkdir -p /app && cp bootstrap/vmctl /app/vmctl && bootstrap/prep_mkosi.sh'


combine multiarch qemu-build:
  extends:
   - .combine multiarch
  variables:
    FDO_REPO_SUFFIX: qemu-build-base
    FDO_DISTRIBUTION_TAG: '2021-07-29.0'
    FDO_ARCHES: x86_64
  needs:
    - build qemu-build image@x86_64