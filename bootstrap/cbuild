#!/bin/bash
#
# Container build script
#
# Invoked by the ci-templates to build a container. See --help output for
# details on the usage.
#
# This script is a copy and cleanup of the YAML files that contained all these
# commands as separate steps in the script: tag of the ci-templates.
# It is in need of cleanup to make it nicer.
#
# Environment variables:
# - CI_*  ... set by the GitLab CI
# - FDO_* ... set by the freedesktop.org CI Templates

# exit on failure
set -e

function die()  {
    echo "$@" >&2
    exit 1
}

###
# Check if we have a container registry for the current run.
#
function check_registry_is_enabled {
    if [[ -z "$CI_REGISTRY_IMAGE" ]]
    then
      # the container registry is not enabled in the project, no point to
      # continue; let the user know so they can fix their project settings
      cat <<EOF > container-build-report.xml ;
    <?xml version="1.0" encoding="utf-8"?>
    <testsuites tests="1" errors="0" failures="1">
      <testsuite name="registry-not-enabled" tests="1" errors="0" failures="1" skipped="0">
        <testcase name="registry-not-enabled">
          <failure message="The container registry is not enabled in $CI_PROJECT_PATH, enable it in the project general settings panel"/>
        </testcase>
      </testsuite>
    </testsuites>
EOF
      die "The container registry is not enabled in $CI_PROJECT_PATH, enable it in the project general settings panel"
    fi
}

###
# Check for a set of known required environment variables and exit if they are missing
#
function check_required_env {
    local required=(
    "FDO_DISTRIBUTION_NAME"
    "FDO_DISTRIBUTION_TAG"
    "CI_PIPELINE_ID"
    "CI_PROJECT_PATH"
    "CI_COMMIT_SHA"
    "CI_REGISTRY_IMAGE"
    "CI_REGISTRY_USER"
    "CI_REGISTRY_PASSWORD"
    )
    local missing=()

    for var in ${required[@]}; do
        if [[ -z "${!var}" ]]
        then
            missing+=( $var )
        fi
    done

    if [[ ${#missing[@]} -gt 0 ]]
    then
        die "Missing environment variables: ${missing[@]}"
    fi
}

###
# Log in to the container registry
function registry_login {
    local user=$1
    local pass=$2
    local registry=$3

    [[ -n "$user" ]] && [[ -n "$pass" ]] && [[ -n "$registry" ]] || die "Insufficient information to log in."

    podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    skopeo login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
}

###
# Check if the distribution image exists.
#
# Returns 0 if the image exists or nonzero otherwise
#
function distro_exists {
    # to be able to test the following script in the CI of the ci-templates
    # project itself, we need to put a special case here to have a
    # different image to pull if it already exists
    local repo_suffix_local=$FDO_REPO_SUFFIX
    if [[ "$repo_suffix_local" == "$FDO_DISTRIBUTION_NAME/ci_templates_test_upstream" ]]
    then
            export FDO_REPO_SUFFIX=${FDO_DISTRIBUTION_NAME}/${FDO_DISTRIBUTION_VERSION}
    fi

    # check if our image is already in the current registry
    # we store the sha of the digest and the layers
    skopeo inspect docker://$CI_REGISTRY_IMAGE/$repo_suffix_local:$FDO_DISTRIBUTION_TAG | jq '[.Digest, .Layers]' > local_sha

    # check if our image is already in the upstream registry
    if [[ -z "$FDO_UPSTREAM_REPO" ]]
    then
      echo "WARNING! Variable \$FDO_UPSTREAM_REPO is undefined, cannot check for images"
    else
        # if the upstream repo has an image, ensure we use the same
        local upstream_registry_path="$(echo $FDO_UPSTREAM_REPO | tr '[:upper:]' '[:lower:]')"
        skopeo inspect docker://$CI_REGISTRY/$upstream_registry_path/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG | jq '[.Digest, .Layers]' > upstream_sha
        # ensure we use the same image from upstream
        if ! diff -u upstream_sha local_sha
        then
            # copy the original image into the current project registry namespace
            # we do 2 attempts with skopeo copy at most
            skopeo copy --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                docker://$CI_REGISTRY/$upstream_registry_path/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG \
                docker://$CI_REGISTRY_IMAGE/$repo_suffix_local:$FDO_DISTRIBUTION_TAG || \
                skopeo copy --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                docker://$CI_REGISTRY/$upstream_registry_path/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG \
                docker://$CI_REGISTRY_IMAGE/$repo_suffix_local:$FDO_DISTRIBUTION_TAG

            return 0
        fi
    fi

    # if we have a local image but none in the upstream repo, use our
    if [[ -s local_sha ]]
    then
        return 0
    fi

    return 1
}

####
# Set up some global variables that contain the various commands we need to prepare images.
# These are by definition specific to the distribution.
#
function setup_pkg_management {
    local distro=$1
    local version=$2

    [[ -n "$distro" ]] && [[ -n "$version" ]] || die "BUG: distro name + version required"

    case "$distro" in
        alpine)
            pkgcmd_upgrade=(
                "apk update"
                "apk upgrade"
                # also, default image doesn't have bash
                "apk add bash"
            )
            pkgcmd_install=(
                "apk add"
            )
            pkgcmd_clean=(
                "rm -f /var/cache/apk/APKINDEX.*"
            )
            ;;
        arch)
            pkgcmd_upgrade=(
                "pacman -S --refresh"
                "pacman -S --sysupgrade --noconfirm"
            )
            pkgcmd_install=(
                "pacman -S --noconfirm"
            )
            pkgcmd_clean=(
                # in case it doesn't exist yet, otherwise pacman clean fails
                "mkdir -p /var/cache/pacman/pkg"
                "pacman -S --clean --noconfirm"
            )
            ;;
        centos)
            if [[ "$version" == [567]* || "$version" == centos[567]* ]]
            then
                DNF="yum"
                DNF_EXTRA_ARGS=""
            else
                DNF="dnf"
                DNF_EXTRA_ARGS="--setopt=install_weak_deps=False"
            fi
            pkgcmd_upgrade=(
                "$DNF upgrade -y $DNF_EXTRA_ARGS"
            )
            pkgcmd_install=(
                "$DNF install -y $DNF_EXTRA_ARGS"
            )
            pkgcmd_clean=(
                "$DNF clean all"
            )
            ;;
        debian)
            pkgcmd_prepare=(
                "cat /etc/apt/sources.list"
                "echo 'path-exclude=/usr/share/doc/*' > /etc/dpkg/dpkg.cfg.d/99-exclude-cruft"
                "echo 'path-exclude=/usr/share/locale/*' >> /etc/dpkg/dpkg.cfg.d/99-exclude-cruft"
                "echo 'path-exclude=/usr/share/man/*' >> /etc/dpkg/dpkg.cfg.d/99-exclude-cruft"
                "echo 'APT::Install-Recommends "false";' > /etc/apt/apt.conf"
                "echo '#!/bin/sh' > /usr/sbin/policy-rc.d"
                "echo 'exit 101' >> /usr/sbin/policy-rc.d"
                "chmod +x /usr/sbin/policy-rc.d"
            )
            pkgcmd_upgrade=(
                "env DEBIAN_FRONTEND=noninteractive apt-get update"
                "env DEBIAN_FRONTEND=noninteractive apt-get -y dist-upgrade"
            )
            pkgcmd_install=(
                "env DEBIAN_FRONTEND=noninteractive apt-get -y install"
            )
            pkgcmd_clean=(
                "env DEBIAN_FRONTEND=noninteractive apt-get clean"
                "rm -f /var/lib/apt/lists/*.lz4"
            )
            ;;
        fedora)
            pkgcmd_upgrade=(
                "dnf upgrade -y --setopt=install_weak_deps=False --nogpgcheck fedora-release fedora-repos*"
                "dnf upgrade -y --setopt=install_weak_deps=False"
            )
            pkgcmd_install=(
                "dnf install -y --setopt=install_weak_deps=False"
            )
            pkgcmd_clean=(
                "dnf clean all"
            )
            ;;
        freebsd)
            pkgcmd_prepare=(
                "mkdir -p /usr/local/etc"
                "echo 'ASSUME_ALWAYS_YES: yes' > /usr/local/etc/pkg.conf"
            )
            pkgcmd_upgrade=(
                "pkg bootstrap -f"
                "pkg update"
                "pkg upgrade -y"
            )
            pkgcmd_install=(
                "pkg install -y"
            )
            pkgcmd_clean=(
                "pkg clean all"
            )
            ;;
        ubuntu)
            pkgcmd_prepare=(
                "echo 'path-exclude=/usr/share/doc/*' > /etc/dpkg/dpkg.cfg.d/99-exclude-cruft"
                "echo 'path-exclude=/usr/share/locale/*' >> /etc/dpkg/dpkg.cfg.d/99-exclude-cruft"
                "echo 'path-exclude=/usr/share/man/*' >> /etc/dpkg/dpkg.cfg.d/99-exclude-cruft"
                "echo 'APT::Install-Recommends "false";' > /etc/apt/apt.conf"
                "echo '#!/bin/sh' > /usr/sbin/policy-rc.d"
                "echo 'exit 101' >> /usr/sbin/policy-rc.d"
                "chmod +x /usr/sbin/policy-rc.d"
            )
            pkgcmd_upgrade=(
                "env DEBIAN_FRONTEND=noninteractive apt-get update"
                "env DEBIAN_FRONTEND=noninteractive apt-get -y dist-upgrade"
            )
            pkgcmd_install=(
                "env DEBIAN_FRONTEND=noninteractive apt-get install -y"
            )
            pkgcmd_clean=(
                "env DEBIAN_FRONTEND=noninteractive apt-get clean"
                "rm -f /var/lib/apt/lists/*.lz4"
            )
            ;;
        *)
            die "Invalid distribution name $distro"
            ;;
    esac
}

###
# Build a containe image and pushes it to the registry. This function is run on
# the current host and uses buildah to build the target image.
#
function build_container {
    local podman_push="${DRY_RUN} podman push"
    local buildah_commit="${DRY_RUN} buildah commit --format docker"
    local buildah_run="buildah run --isolation chroot"
    local buildah_from="buildah from --isolation=chroot"
    local cache_dir=${FDO_CACHE_DIR:-/cache}
    local base_image="$FDO_BASE_IMAGE"

    if [[ -z "$FDO_BASE_IMAGE" ]]
    then
        case "$FDO_DISTRIBUTION_NAME" in
            alpine)
                base_image=alpine:latest
                ;;
            arch)
                base_image=archlinux/archlinux
                ;;
            *)
                if [[ -z "$FDO_DISTRIBUTION_VERSION" ]]
                then
                    echo "Required variable FDO_DISTRIBUTION_VERSION is not set"
                    exit 1
                fi
                case "$FDO_DISTRIBUTION_NAME" in
                    centos)
                        base_image=quay.io/centos/centos:$FDO_DISTRIBUTION_VERSION
                        ;;
                    fedora)
                        base_image=registry.fedoraproject.org/fedora:$FDO_DISTRIBUTION_VERSION
                        ;;
                    *)
                        base_image="$FDO_DISTRIBUTION_NAME:$FDO_DISTRIBUTION_VERSION"
                        ;;
                esac
            ;;
        esac
    fi

    echo "Building $FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG from $base_image"
    # initial set up: take the base image, update it and install the packages
    local buildcntr=$($buildah_from $base_image)
    local buildmnt=$(buildah mount $buildcntr)

    # Execute the distribution-specific prepare/upgrade/install commands
    for cmd in "${pkgcmd_prepare[@]}"
    do
        ${cmd/ \// $buildmnt/}  # Swap any abs path to run in the build mount
    done
    for cmd in "${pkgcmd_upgrade[@]}"
    do
        $buildah_run $buildcntr $cmd
    done

    if [[ -n "$FDO_DISTRIBUTION_PACKAGES" ]]
    then
        for cmd in "${pkgcmd_install[@]}"
        do
            $buildah_run $buildcntr ${cmd} $FDO_DISTRIBUTION_PACKAGES
        done
    fi

    # check if there is an optional post install script and run it
    if [[ -n "$FDO_DISTRIBUTION_EXEC" ]]
    then
        echo Running $FDO_DISTRIBUTION_EXEC
        mkdir $buildmnt/tmp/clone
        pushd $buildmnt/tmp/clone
        git init
        git remote add origin $CI_REPOSITORY_URL
        git fetch --depth 1 origin $CI_COMMIT_SHA
        git checkout FETCH_HEAD  > /dev/null
        buildah config --workingdir /tmp/clone --env HOME="$HOME" $buildcntr
        if [ -e $cache_dir ]
        then
            CACHE="-v $cache_dir:/cache:rw,shared,z"
        fi
        $buildah_run $CACHE $buildcntr bash -c "set -x ; $FDO_DISTRIBUTION_EXEC"
        popd
        buildah config --workingdir / --env HOME="$HOME" $buildcntr
        rm -rf $buildmnt/tmp/clone
    fi

    # Execute the distribution-specific cleanup commands
    # (e.g. clean up packages database)
    for cmd in "${pkgcmd_clean[@]}"
    do
        $buildah_run $buildcntr ${cmd}
    done

    # set up the working directory
    mkdir -p $buildmnt/app
    buildah config --workingdir /app $buildcntr
    # umount the container, not required, but, heh
    buildah unmount $buildcntr

    # Apply the custom fdo labels
    apply_container_labels $buildcntr

    # tag the current container
    $buildah_commit $buildcntr $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG

    # clean up the working container
    buildah rm $buildcntr

    # push the container image to the registry
    # There is a bug when pushing 2 tags in the same repo with the same base:
    # this may fail. Just retry it after.
    $podman_push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG || true
    sleep 2
    $podman_push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG
}

function swap_image_for_bsd {
    local freebsd_version="$1"

    [[ -n "$freebsd_version" ]] || die "Missing argument: freebsd version"

    local imgname="FreeBSD-$freebsd_version-RELEASE-amd64-BASIC-CI.raw.xz"

    # Don't re-download when we're running locally
    if ! [[ -f "$imgname" ]]
    then
        local url="https://download.freebsd.org/ftp/releases/CI-IMAGES/$freebsd_version-RELEASE/amd64/Latest/$imgname"
        curl $url > $imgname
    fi

    cp $imgname /app/image.raw.xz

    # We just swapped out the image so connecting to it with existing
    # known_hosts will fail. Reset the entries
    /app/vmctl reset-known-hosts
}


###
# Run qemu for /app/image.raw and build the target distribution in that image using mkosi.
#
# This function starts up the pre-prepared qemu image (see vmctl) and then runs
# mkosi inside that qemu instance.
#
# Running this function locally requires some custom setup on the host to match
# the qemu-build-base image, specifically:
# /app/vmctl must be the vmctl script (just symlink to the git one)
# /app/image.raw[.xz] is the pre-prepared qemu image to run mkosi in
# the target ssh keys must be installed in the user's .ssh directory (grab them
# from the qemu-build-base container)
#
function build_qemu_mkosi {
    local qemu_packages=""
    local ssh_pubkey="$1"

    [[ -n "$ssh_pubkey" ]] || die "Missing ssh public key file"

    local qemu_version=$FDO_DISTRIBUTION_VERSION
    if [[ $FDO_DISTRIBUTION_NAME == 'ubuntu' ]]; then
        local versions=()
        # Bash can't do numeric associative arrays so we have to mess with
        # the versions format
        versions["v16_04"]="xenial"
        versions["v16_10"]="yakkety"
        versions["v17_04"]="zesty"
        versions["v17_10"]="artful"
        versions["v18_04"]="bionic"
        versions["v18_10"]="cosmic"
        versions["v19_04"]="disco"
        versions["v19_10"]="eoan"
        versions["v20_04"]="focal"
        versions["v20_10"]="groovy"
        versions["v21_04"]="hirsute"
        local version="v${FDO_DISTRIBUTION_VERSION/./_}"
        qemu_version="${versions[$version]}"
    fi

    # The packages required on the qemu image
    case "$FDO_DISTRIBUTION_NAME" in
        alpine)
            # no qemu support yet
            ;;
        arch)
            # no qemu support yet
            ;;
        centos)
            # no qemu support yet
            ;;
        fedora)
            qemu_packages=(
                "openssh-clients"
                "openssh-server"
                "NetworkManager"
                "iproute"
                "iputils"
                "git-core"
            )
            ;;
        debian)
            qemu_packages=(
                "openssh-client"
                "openssh-server"
                "network-manager"
                "iproute2"
                "git"
            )
            ;;
        ubuntu)
            qemu_packages=(
                "openssh-client"
                "openssh-server"
                "network-manager"
                "iproute2"
                "git"
            )
            qemucmds_finalize=(
                "mkdir -p mkosi.extra/etc/NetworkManager/conf.d/"
                "touch mkosi.extra/etc/NetworkManager/conf.d/10-globally-managed-devices.conf"
            )
            ;;
        *)
            die "Invalid distribution name $distro"
            ;;
    esac

    PATH="/app:$PATH"

    # start our current base mkosi image
    /app/vmctl start -cdrom /app/my-seed.iso

    # Does nothing in the CI but makes re-running this script locally easier
    /app/vmctl exec rm -rf mkosi.extra
    /app/vmctl exec rm -rf mkosi.default
    /app/vmctl exec rm -rf mkosi.cache
    /app/vmctl exec rm -rf loop

    cat <<EOF > mkosi.default
[Distribution]
Distribution=$FDO_DISTRIBUTION_NAME
Release=$qemu_version

[Output]
Format=gpt_ext4
Bootable=yes
BootProtocols=bios
WithUnifiedKernelImages=no
Password=root
KernelCommandLine=!* selinux=0 audit=0 rw console=tty0 console=ttyS0 systemd.journald.forward_to_console=1

[Partitions]
RootSize=2G

[Packages]
# The packages to appear in both the build and the final image
Packages=
EOF
    for p in "${qemu_packages[@]}"
    do
        echo "    $p" >> mkosi.default
    done
    echo $FDO_DISTRIBUTION_PACKAGES | tr ' ' '\n' | sed -e 's/^/    /' >> mkosi.default

    # Add our pubkey to the target image
    mkdir -p mkosi.extra/root/.ssh
    chmod 700 mkosi.extra/root/.ssh
    cp $ssh_pubkey mkosi.extra/root/.ssh/authorized_keys
    chmod 600 mkosi.extra/root/.ssh/authorized_keys
    # enable sshd on the target
    mkdir -p mkosi.extra/etc/systemd/system/multi-user.target.wants
    for cmd in "${qemucmds_finalize[@]}"
    do
        $cmd
    done
    scp mkosi.default vm:/root/mkosi.default
    scp mkosi.default vm:/root/mkosi.default
    scp -r mkosi.extra vm:/root/
    rm -rf mkosi.extra
    rm mkosi.default

    # enable sshd on the target (may fail if it exists)
    /app/vmctl exec ln -s /usr/lib/systemd/system/sshd.service mkosi.extra/etc/systemd/system/multi-user.target.wants/sshd.service

    # create a cache folder (useful only when manually testing this script)
    /app/vmctl exec mkdir mkosi.cache

    # Hack around debootstrap needing an update for every Ubuntu version
    # and lagging behind releases. This causes debootstrap to fail because it
    # can't find the repos. Everything links to gutsy now anyway so we can force
    # that symlink into place where upstream debootstrap doesn't supply it.
    if [[ "$FDO_DISTRIBUTION_NAME" == "ubuntu" ]]
    then
        /app/vmctl exec ln -sf /usr/share/debootstrap/scripts/gutsy /usr/share/debootstrap/scripts/$qemu_version ;
    fi

    # run mkosi in the VM!
    /app/vmctl exec mkosi

    # mount the root partition locally to extract the kernel and initramfs
    /app/vmctl exec mkdir loop
    offset=$(/app/vmctl exec fdisk -l image.raw | grep image.raw2 | cut -d ' ' -f 3)
    /app/vmctl exec mount -o ro,loop,offset=$(($offset * 512)) image.raw loop/

    # fetch kernel and initramfs
    /app/vmctl exec ls loop/boot/
    /app/vmctl exec "cp loop/boot/vmlinuz* loop/boot/initr* ."

    /app/vmctl exec umount loop/

    # fetch the image and kernel and store in results dir
    rm -rf build_qemu_results/
    mkdir -p build_qemu_results/
    scp vm:image.raw build_qemu_results/
    scp vm:vmlinuz\* vm:initr\* build_qemu_results/
    /app/vmctl exec rm image.raw

    # terminate qemu
    /app/vmctl stop

    # compress the image, most of it is zeroes after all
    xz -T0 build_qemu_results/image.raw
}

##
# Install the given ssh key as authorized key for our vm image
#
# This currently assumes that the image accepts ssh logins without a
# password to actually install that key.
#
function install_ssh_keys {
    local ssh_pubkey="$1"

    export VMCTL_SSH_OPTS="-oPreferredAuthentications=password -oIdentitiesOnly=no"
    set -e
    /app/vmctl start
    /app/vmctl exec mkdir -p /root/.ssh
    /app/vmctl exec chmod 700 /root/.ssh
    /app/vmctl exec chmod 700 /root/.ssh

    scp "$ssh_pubkey" vm:/root/.ssh/authorized_keys
    /app/vmctl exec chmod 600 /root/.ssh/authorized_keys
    /app/vmctl stop
    set +e
    unset VMCTL_SSH_OPTS
}

##
# Start our image, then prep and install everything in that image.
#
function build_qemu_in_qemu {
    local ssh_pubkey="$1"

    set -e
    /app/vmctl start

    for cmd in "${pkgcmd_prepare[@]}"
    do
        /app/vmctl exec $cmd
    done
    for cmd in "${pkgcmd_upgrade[@]}"
    do
        /app/vmctl exec $cmd
    done

    if [[ -n "$FDO_DISTRIBUTION_PACKAGES" ]]
    then
        for cmd in "${pkgcmd_install[@]}"
        do
            /app/vmctl exec $cmd $FDO_DISTRIBUTION_PACKAGES
        done
    fi

    for cmd in "${pkgcmd_clean[@]}"
    do
        /app/vmctl exec $cmd
    done

    /app/vmctl stop

    # recompress image
    xz -T0 /app/image.raw

    # store image in results dir
    rm -rf build_qemu_results/
    mkdir -p build_qemu_results/
    mv /app/image.raw.xz build_qemu_results/
    set +e
}

###
# Build a qemu-capable container image.
#
# This function starts up the pre-prepared qemu image (see vmctl) and then runs
# mkosi inside that qemu instance.
#
# Running this function locally requires some custom setup on the host to match
# the qemu-build-base image, specifically:
# /app/vmctl must be the vmctl script (just symlink to the git one)
# /app/image.raw[.xz] is the pre-prepared qemu image to run mkosi in
# the target ssh keys must be installed in the user's .ssh directory (grab them
# from the qemu-build-base container)
#
function build_qemu {
    local podman_push="${DRY_RUN} podman push"
    local buildah_from="buildah from --isolation=chroot"
    local buildah_commit="${DRY_RUN} buildah commit --format docker"

    if pgrep qemu > /dev/null; then
        echo "qemu is already running"
        exit 1
    fi

    if [[ ! -e "/app/vmctl" ]]; then
        echo "/app/vmctl script is required"
        exit 1
    fi

    if [[ -z "$FDO_CI_TEMPLATES_QEMU_BASE_IMAGE" ]]; then
        die "FDO_CI_TEMPLATES_QEMU_BASE_IMAGE is required"
    fi

    # create a new ssh key
    sshdir=$(mktemp -d)
    ssh-keygen -t ed25519 -f $sshdir/ci-templates-vm -N ''

    if [[ "$FDO_DISTRIBUTION_NAME" == "freebsd" ]]; then
        swap_image_for_bsd $FDO_DISTRIBUTION_VERSION
        install_ssh_keys $sshdir/ci-templates-vm.pub
        build_qemu_in_qemu $sshdir/ci-templates-vm.pub
    else
        build_qemu_mkosi $sshdir/ci-templates-vm.pub
    fi

    # building the final image
    storageconf=/etc/containers/storage.conf
    if [[ -w "$storageconf" ]]; then
        cat > $storageconf <<EOF
[storage]
driver = "vfs"
runroot = "/var/run/containers/storage"
graphroot = "/var/lib/containers/storage"
EOF
    fi

    echo "Building $FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG from $FDO_CI_TEMPLATES_QEMU_BASE_IMAGE"

    set -e
    buildcntr=$($buildah_from $FDO_CI_TEMPLATES_QEMU_BASE_IMAGE)
    buildmnt=$(buildah mount $buildcntr)

    # insert our final VM image we just built
    mkdir -p $buildmnt/app
    mv build_qemu_results/* $buildmnt/app/

    # Install our ssh key
    mkdir -p $buildmnt/root/.ssh
    chmod 700 $buildmnt/root/.ssh
    cp $sshdir/ci-templates-vm* $buildmnt/root/.ssh/

    # umount the container, not required, but, heh
    buildah unmount $buildcntr

    # Apply the custom fdo labels
    apply_container_labels $buildcntr

    # tag the current container
    $buildah_commit $buildcntr $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG

    # clean up the working container
    buildah rm $buildcntr

    # push the container image to the registry
    # There is a bug when pushing 2 tags in the same repo with the same base:
    # this may fail. Just retry it after.
    $podman_push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG || true
    sleep 2
    $podman_push $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG
    set +e
}

###
# Apply a set of container labels that are common to all fdo containers
# produced by the templates. These labels allow us to identify when a container
# was built and what for, including whether we can clean it up and/or archive
# it.
function apply_container_labels {
    buildcntr="$1"

    echo "Applying fdo labels to $buildcntr"
    if [[ -n "$FDO_EXPIRES_AFTER" ]]
    then
        buildah config -l fdo.expires-after=$FDO_EXPIRES_AFTER $buildcntr
    fi

    if [[ -n "$FDO_UPSTREAM_REPO" ]]
    then
        buildah config -l fdo.upstream-repo=$FDO_UPSTREAM_REPO $buildcntr
    fi

    buildah config -l fdo.pipeline_id="$CI_PIPELINE_ID" $buildcntr
    buildah config -l fdo.job_id="$CI_JOB_ID" $buildcntr
    buildah config -l fdo.project="$CI_PROJECT_PATH" $buildcntr
    buildah config -l fdo.commit="$CI_COMMIT_SHA" $buildcntr
}

function usage {
    echo "Usage: $0  [OPTIONS] build-container|build-qemu <distroname> <version> <tag>"
    echo ""
    echo "Builds a container or qemu image for the given distribution version and"
    echo "commits it to the registry with the given tag."
    echo ""
    echo "Options:"
    echo "   --dry-run          Build images but do not commit to the registry"
    echo "   --debug            Enable debugging output"
    echo ""
    echo "Project-specific options:"
    echo "   --user <string>      CI registry username to log use (default: $USER)"
    echo "   --password <string>  CI registry password"
    echo "   --project <string>   GitLab project path (default: $USER/$(basename "$PWD"))"
    echo "   --git-sha <string>   Git commit SHA (default: HEAD of \$PWD))"
    echo "   --registry <string>  CI registry (default: registry.freedesktop.org))"
    echo "   --pipeline <string>  CI pipeline id (default: 1111))"
    echo "   --packages <string>  A space-separated list of packages to install"
    echo ""
    echo "   The above options default to the GitLab environment variables if set, "
    echo "   or defaults as listed above otherwise"
    echo ""
}

TEMP=$(getopt -o 'h' --long 'help,debug,dry-run,user:,password:,project,:git-sha:,registry:,pipeline-id:,packages:,' -- "$@")
if [[ $? -ne 0 ]]; then
    exit 1
fi
eval set -- "$TEMP"
unset TEMP
while true
do
    case "$1" in
        --dry-run)
            DRY_RUN="echo"
            shift
            ;;
        --debug)
            set -x
            shift
            ;;
        --user)
            CI_REGISTRY_USER=$2
            shift 2
            ;;
        --password)
            CI_REGISTRY_PASSWORD=$2
            shift 2
            ;;
        --project)
            CI_PROJECT_PATH=$2
            shift 2
            ;;
        --packages)
            FDO_DISTRIBUTION_PACKAGES="$2"
            shift 2
            ;;
        --registry)
            CI_REGISTRY=$2
            shift 2
            ;;
        --git-sha)
            CI_COMMIT_SHA=$2
            shift 2
            ;;
        --pipeline-id)
            CI_PIPELINE_ID=$2
            shift 2
            ;;
        --)
            shift
            break
            ;;
        -h|--help)
            usage
            exit 0
            ;;
        *)
            usage;
            exit 1;
            ;;
    esac
done

# Required arguments: command distribution version tag
[[ $# -ge 4 ]] || (usage; exit 1)
cbuild_cmd=$1
shift
[[ $# -ge 3 ]] || (usage; exit 1)
FDO_DISTRIBUTION_NAME=$1
shift
[[ $# -ge 2 ]] || (usage; exit 1)
FDO_DISTRIBUTION_VERSION=$1
shift
[[ $# -ge 1 ]] || (usage; exit 1)
FDO_DISTRIBUTION_TAG=$1
shift

# If we are running in the CI, all the variables below are set. Otherwise,
# auto-fill some of the easy ones.
if [[ -z "$CI" ]]
then
    USER="${USER:-$(whoami)}"
    CI_REGISTRY_USER="${CI_REGISTRY_USER:-$USER}"
    CI_PROJECT_PATH="${CI_PROJECT_PATH:-$CI_REGISTRY_USER/$(basename $PWD)}"
    CI_COMMIT_SHA="${CI_COMMIT_SHA:-$(git rev-list --max-count=1 HEAD)}"
    CI_REGISTRY="${CI_REGISTRY:-registry.freedesktop.org}"
    CI_REGISTRY_IMAGE="${CI_REGISTRY_IMAGE:-$CI_REGISTRY/$CI_PROJECT_PATH}"
    CI_PIPELINE_ID="${CI_PIPELINE_ID:-1111}"
    if [[ -z "$FDO_CI_TEMPLATES_QEMU_BASE_IMAGE" ]]; then
        # default to the most recent qemu-base image
        quay_url="quay.io/freedesktop.org/ci-templates"
        base_img=$(skopeo list-tags docker://$quay_url | jq -r ".Tags[]" | grep qemu-base | sort -r | head -n 1)
        FDO_CI_TEMPLATES_QEMU_BASE_IMAGE="$quay_url:$base_img"
    fi
fi

check_required_env
setup_pkg_management $FDO_DISTRIBUTION_NAME $FDO_DISTRIBUTION_VERSION

if [[ -z "$FDO_REPO_SUFFIX" ]]
then
    if [[ -z "$FDO_DISTRIBUTION_VERSION" ]]
    then
        echo "Required variable FDO_DISTRIBUTION_VERSION is not set"
        exit 1
    fi
    FDO_REPO_SUFFIX=$FDO_DISTRIBUTION_NAME/$FDO_DISTRIBUTION_VERSION
fi

if [[ -z "$FDO_DISTRIBUTION_EXEC" ]] && [[ -z "$FDO_DISTRIBUTION_PACKAGES" ]]
then
    echo "WARNING: FDO_DISTRIBUTION_PACKAGES and FDO_DISTRIBUTION_PACKAGES are both unset."
fi

check_registry_is_enabled
registry_login "$CI_REGISTRY_USER" "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"

if [[ -z "$FDO_FORCE_REBUILD" ]]
then
    if distro_exists
    then
        echo "Distro exists"
        exit 0
    fi
fi

case "$cbuild_cmd" in
    build-container)
        build_container
        ;;
    build-qemu)
        build_qemu
        ;;
    *)
        echo "Unknown command type, choose one of 'build-container' or 'build-qemu'"
        exit 1
        ;;
esac

cat <<EOF > container-build-report.xml ;
<?xml version="1.0" encoding="utf-8"?>
<testsuites tests="1" errors="0" failures="0">
  <testsuite name="$build_cmd" tests="1" errors="0" failures="0" skipped="0">
    <testcase classname="$build_cmd" name="$build_cmd" />
 </testsuite>
</testsuites>
EOF
